# sonar-scanner

SonarQube Sonar Scanner with the following modifications:
- Added own Certificate Authority (CA) certificates to allow use of internal ressources

Do you want to use your own certificates? Simply edit Dockerfile and import your Root & Intermediate CA files.

## GitLab CI
The following example describes how the Sonar Scanner can be used for GitLab Runner / GitLab CI projects.

Use the following configuration for `.gitlab-ci.yml`:
```yaml
sonarqube-check:
  stage: analysis
  image:
    name: registry.gitlab.com/bella.network/sonar-scanner:latest
    entrypoint: [""]
  variables:
    GIT_DEPTH: 0
    SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"
  cache:
    key: "${CI_JOB_NAME}"
    paths:
      - .sonar/cache
  dependencies:
    - test
  script:
    - sonar-scanner
  allow_failure: true
```

Add the following file named `sonar-project.properties` in the root directory of your project:
```
sonar.projectKey=<your-project-key>
sonar.qualitygate.wait=true

sonar.sources=.
sonar.exclusions=**/*_test.go,coverage/**,report.xml,test-report.out,coverage.out

sonar.tests=.
sonar.test.inclusions=**/*_test.go

sonar.sourceEncoding=UTF-8

sonar.scm.provider=git
sonar.go.coverage.reportPaths=coverage.out
sonar.go.golangci-lint.reportPaths=report.xml
```

Add the following environment variables to your GitLab project or group:
```
SONAR_HOST_URL=https://your.sonarqube.instance
```

Using the configuration above, you can use an internal SonarQube using an internal or self signed certificate.
