FROM openjdk:21-jdk-slim AS builder

ADD http://ca.bella.pm/root_ca.crt /tmp/
ADD http://ca.bella.pm/intermediate_ca.crt /tmp/

RUN keytool -import -v -trustcacerts -alias bn_root_ca -file /tmp/root_ca.crt \
    -keystore /usr/local/openjdk-21/lib/security/cacerts -noprompt -storepass changeit

RUN keytool -import -v -trustcacerts -alias bn_web_intermediate -file /tmp/intermediate_ca.crt \
    -keystore /usr/local/openjdk-21/lib/security/cacerts -noprompt -storepass changeit

FROM sonarsource/sonar-scanner-cli:latest
COPY --from=builder /usr/local/openjdk-21/lib/security/cacerts /etc/ssl/certs/java/cacerts
